const origen = document.getElementById("origen");
const destino = document.getElementById("destino");
     
		origen.addEventListener("change", function() {
			const selectedValue = origen.value;
			const options = destino.options;
			for (let i = 0; i < options.length; i++) {
				if (options[i].value === selectedValue) {
					options[i].style.display = "none";
				} else {
					options[i].style.display = "";
				}
			}
		});
        //codificar el boton calcular
        const calcular = document.getElementById("btnCalcular");
        calcular.addEventListener("click", function() {
    
        //obtener los valores de los inputs text
        const cantidad = parseFloat(document.getElementById("cantidad").value);
        const origen = parseInt(document.getElementById("origen").value);
        const destino = parseInt(document.getElementById("destino").value);
      
         //realiza los calculos
        const tasaMXN = 19.85;
        const tasaUSD = 1;
        const tasaCAD = 1.35;
        const tasaEUR = 0.99;
        
        let subtotal;
    
          switch (origen) {
            case 1: // Dolar Estadounidense
              switch (destino) {
                case 1: // Dolar Estadounidense
                  subtotal = cantidad * tasaUSD;
                  break;
                case 2: // Peso Mexicano
                  subtotal = cantidad * tasaMXN;
                  break;
                case 3: // Dolar Canadiense
                  subtotal = cantidad * tasaCAD;
                  break;
                case 4: // Euro
                  subtotal = cantidad * tasaEUR;
                  break;
              }
              break;
            case 2: // Peso Mexicano
              switch (destino) {
                case 1: // Dolar Estadounidense
                  subtotal = cantidad / tasaMXN;
                  break;
                case 2: // Peso Mexicano
                  subtotal = cantidad * tasaUSD / tasaMXN;
                  break;
                case 3: // Dolar Canadiense
                  subtotal = cantidad / tasaMXN * tasaCAD;
                  break;
                case 4: // Euro
                  subtotal = cantidad / tasaMXN * tasaEUR;
                  break;
              }
              break;
            case 3: // Dolar Canadiense
              switch (destino) {
                case 1: // Dolar Estadounidense
                  subtotal = cantidad / tasaCAD;
                  break;
                case 2: // Peso Mexicano
                  subtotal = cantidad / tasaCAD * tasaMXN;
                  break;
                case 3: // Dolar Canadiense
                  subtotal = cantidad * tasaUSD;
                  break;
                case 4: // Euro
                  subtotal = cantidad / tasaCAD * tasaEUR;
                  break;
              }
              break;
            case 4: // Euro
              switch (destino) {
                case 1: // Dolar Estadounidense
                  subtotal = cantidad / tasaEUR;
                  break;
                case 2: // Peso Mexicano
                  subtotal = cantidad / tasaEUR * tasaMXN;
                  break;
                case 3: // Dolar Canadiense
                  subtotal = cantidad / tasaEUR * tasaCAD;
                  break;
                case 4: //Euro
                  subtotal = cantidad * tasaUSD / tasaEUR;
                break;
                }
        break;
    
        }
        const comision = subtotal * 0.03;
        const total = subtotal + comision;
    
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        document.getElementById("comision").value = comision.toFixed(2);
        document.getElementById("totalPagar").value = total.toFixed(2);
      });

      //codificar la funcion registro
      function agregarRegistro() {
        const cantidad = parseInt(document.getElementById("cantidad").value);
        const origen = parseInt(document.getElementById("origen").value);
        const destino = parseInt(document.getElementById("destino").value);
        const subtotal = parseFloat(document.getElementById("subtotal").value);
        const comision = parseFloat(document.getElementById("comision").value);
        const totalPagar = parseFloat(document.getElementById("totalPagar").value);
    
        let origenTxt;
        let destinoTxt;
    
        switch(origen){
            case 1:
                origenTxt="Dolar Estadounidense";
                break;
            case 2:
                origenTxt="Pesos Mexicanos";
                break;
            case 3:
                origenTxt="Dolar Canadiense";
                break;
            case 4:
                origenTxt="Euro";
                break;
        }
        switch(destino){
            case 1:
                destinoTxt="Dolar Estadounidense";
                break;
            case 2:
                destinoTxt="Pesos Mexicanos";
                break;
            case 3:
                destinoTxt="Dolar Canadiense";
                break;
            case 4:
                destinoTxt="Euro";
                break;
        }
        const tabla = document.getElementById("registros");
        const newRow = `
        <tr>
          <td>${cantidad}</td>
          <td>${origenTxt}</td>
          <td>${destinoTxt}</td>
          <td>${subtotal.toFixed(2)}</td>
          <td>${comision.toFixed(2)}</td>
          <td>${totalPagar.toFixed(2)}</td>
        </tr>
      `;
      
        tabla.innerHTML += newRow;
    
        let totalGeneral = 0;
        const filas = tabla.getElementsByTagName("tr");
      
        for (let i = 1; i < filas.length; i++) {
          const totalPagar = parseFloat(filas[i].getElementsByTagName("td")[5].textContent);
          totalGeneral += totalPagar;
        }
      
        document.getElementById("total").textContent = "$"+totalGeneral.toFixed(2);
      }

      //Eliminar Registro
      function eliminarRegistro(){
        const tabla = document.getElementById("registros");
        let numRows = tabla.rows.length;
        for (let i = numRows - 1; i > 0; i--) {
            tabla.deleteRow(i);
        }
        let totalGeneral=0;
        document.getElementById("total").textContent = totalGeneral;
      }

      


